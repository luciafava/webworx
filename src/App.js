import React from 'react';
import Navbar from './components/Layout/Navbar';
import Header from './components/Layout/Header/Header';
import Info from './components/Layout/Info/Info';
import Features from './components/Layout/Features/Features';
import Footer from './components/Layout/Footer';

import './App.css';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Header />
      <Info />
      <Features />
      <Footer />
    </div>
  );
}

export default App;
