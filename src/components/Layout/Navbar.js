import React from 'react';

export default function Navbar() {
    return (
        <div className="navbar">
            <div className="navbar__container navbar__flex">
                <h2 className="navbar__logo">WebWorx</h2>
                <nav>
                    <ul className="navbar__items">
                        <li><a href="#showcase">Showcase</a></li>
                        <li><a href="#pricing">Pricing</a></li>
                        <li><a href="#signin">Sign in</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    )
}

