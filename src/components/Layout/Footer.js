import React from 'react'

export default function Footer() {
    return (
        <div className="footer footer__wrapper">
            <div className="container flex">
                <div>
                    <h2>WebWorx</h2>
                    <p>Copyright &copy; 2021</p>
                </div>
                <nav>
                    <ul className="link__items">
                        <li>Terms and Conditions</li>
                        <li>Privacy</li>
                        <li>Cookie Policy</li>
                    </ul>
                </nav>

            </div>
        </div>
    )
}
