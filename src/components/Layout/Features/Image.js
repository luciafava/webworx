import React from 'react';
import image from '../images/image2.png';

export default function Image() {
    return (
        <div>
            <img className="image my-3" src={image} alt="image02" />
        </div>
    )
}
