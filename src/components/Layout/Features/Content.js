import React from 'react'

export default function Content() {
    return (
        <div>
            <p className="lead my-1">
            <span className="lead__01">WebWorx</span> is a powerful landing page builder used by over 7,000 startups, 
            entrepreneurs and hackers.
            </p>
            <a href="#features" className="button button__primary">Create website</a>
        </div>
    )
}
