import React from 'react';
import Content from './Content';
import Image from './Image';

export default function Features() {
    return (
        <div className="features bg_light">
            <div className="wrapper">
                <div className="container flex">
                <Content />
                <Image />
                </div>
            </div>
        </div>
    )
}
