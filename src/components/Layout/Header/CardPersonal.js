import React from 'react';

export default function CardPersonal() {
    return (
        <div className="showcase showcase__flex">
            <div className="container">
                <div className="showcase__title">
                    <h1>Build amazing Landing Page on your own</h1>
                </div>
                <div className="showcase__text">
                    <p>
                    The simplest drag and drop landing page builder for startups, mobile apps, ad Saas. No code.
                    </p>
                </div>
                <a href="#home" className="button my-1">Create website</a>
            </div>
        </div>
    )
}
