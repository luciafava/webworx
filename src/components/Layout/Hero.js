import React from 'react';
import Header from './Header/Header';

export default function Hero() {
    return (
        <div>
            <Header />
            <h1>Hello world!</h1>
        </div>
    )
}
