import React from 'react';
import image from '../images/image.jpg';

export default function Image() {
    return (
        <div>
            <img className="image" src={image} alt="image2" />
        </div>
    )
}
