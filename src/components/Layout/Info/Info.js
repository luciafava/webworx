import React from 'react'
import Content from './Content';
import Image from './Image';

export default function Info() {
    return (
        <div className="info info__component wrapper">
            <div className="container">
                <div className="info__flex">
                <Image />
                <Content />
                </div>
            </div>
        </div>
    )
}
