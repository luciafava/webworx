import React from 'react';

export default function Content() {
    return (
        <div>
            <div className="card__group">
                <div className="card">
                    <div className="card__title">
                        <h3>Edit Pages Online</h3>
                    </div>
                    <div className="card__description">
                        Use the online editor to change text, images, buttons and forms
                    </div>
                    <div className="card__description">
                        Use the online editor to change text, images, buttons and forms
                    </div>
                </div>
                <div className="card">
                    <div className="card__title">
                        <h3>Customize It</h3>
                    </div>
                    <div className="card__description">
                        Change colors palettes, fonts, button styles and backgrounds.
                    </div>
                </div>
                <div className="card">
                    <div className="card__title">
                        <h3>Publish on Domain</h3>
                    </div>
                    <div className="card__description">
                        Launch your website on a custom domain by clicking a slingle button.
                    </div>
                </div>
                <div className="card">
                    <div className="card__title">
                        <h3>Enjoy components collection</h3>
                    </div>
                    <div className="card__description">
                        Discover the fabulous components gallery.
                    </div>
                </div>
            </div>
        </div>
    )
}
